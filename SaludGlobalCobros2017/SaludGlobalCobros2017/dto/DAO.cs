﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using SaludGlobalCobros2017.model;
namespace SaludGlobalCobros2017.dto {
    class DAO {
        private DataTable dt;
        private MsSQL cnx;
        private SqlDataReader reader;
        private Respuesta respuesta;
        private int TimeOUtSQL =600000;
        private String query;
        public DAO(){
            cnx = new MsSQL();
            respuesta = new Respuesta();
        }


       public DataTable get_all_clients(String Today){
            query = " EXEC dbo.SP_sel_generacobro2 @fechaactual = '"+Today+"' -- varchar(10)";
            dt = ReaderSQL(query);
            return dt;
        }
        /// <summary>
        ///  Devuelve todos los horarios en los que se debe cobrar
        /// </summary>
        /// <param name="hora"></param>
        /// <param name="Minuto"></param>
        /// <returns></returns>
        public DataTable get_all_schedule(String hora, String Minuto)
        {
            query = " select count(*) respuesta  from tbl_ama_HorarioPaises a " +
                        "inner join Tbl_AMA_Paises b " +
                        " on a.Pais_id =b.Pais_Id " +
                        "where Hora in('" + hora + ":" + Minuto + "') and Status = 1" +
                        " and Robot ='S' ";
            dt = ReaderSQL(query);
            return dt;
        }
        
       public DataTable ReaderSQL(String query)
        {
            DataTable table = new DataTable();

            try{
                cnx.conexion.Open();
                cnx.cmd = new SqlCommand(query, cnx.conexion);
                cnx.cmd.CommandTimeout = TimeOUtSQL;
                reader = cnx.cmd.ExecuteReader();
                if (reader.HasRows) { 
                    table.Load(reader);
                }
                cnx.conexion.Close();
            }catch (Exception ex) {
                Console.WriteLine(ex);
                //table.Load(reader);
                cnx.conexion.Close();
                throw;
            }

            return table;           
        }
        
        public Respuesta  DoSQL( String query )
        {   
            try{
                cnx.conexion.Open();
                cnx.cmd = new SqlCommand(query, cnx.conexion);
                cnx.cmd.CommandTimeout = TimeOUtSQL;
                cnx.cmd.ExecuteNonQuery();
                cnx.conexion.Close();
                respuesta.Code = "OK";
                respuesta.Msg = "Operacion Realizada exitosamente";

            }catch{
                cnx.conexion.Close();
                respuesta.Code = "KO";
                respuesta.Msg = "Ocurrio un error durante la ejecucion de la consulta";
            }

            return respuesta;
        } 
                   

    }
}
